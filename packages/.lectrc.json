{
  "babelrc": {
    "cli": {
      "presets": [
        [
          "@babel/preset-env",
          {
            "modules": false,
            "targets": {
              "node": "4.0"
            }
          }
        ]
      ]
    },
    "set": {
      "env": {
        "test": {
          "plugins": [
            "istanbul"
          ]
        }
      },
      "presets": [
        [
          "@babel/preset-env",
          {
            "modules": false
          }
        ]
      ]
    }
  },
  "badges": [
    {
      "node": {
        "alt": "Minimum Node version required",
        "img": "https://img.shields.io/node/v/%REPONAME%.svg?style=flat-square&label=works%20on%20node",
        "url": "https://www.npmjs.com/package/%REPONAME%"
      }
    },
    {
      "bitbucket": {
        "alt": "Repository is on BitBucket",
        "img": "https://img.shields.io/badge/repo-on%20BitBucket-brightgreen.svg?style=flat-square",
        "url": "https://bitbucket.org/%USERNAME%/%REPONAME%"
      }
    },
    {
      "cov": {
        "alt": "Coverage",
        "img": "https://img.shields.io/badge/coverage-%COVPERC%-%COVBADGECOLOR%.svg?style=flat-square",
        "url": "https://bitbucket.org/%USERNAME%/%REPONAME%"
      }
    },
    {
      "deps2d": {
        "alt": "View dependencies as 2D chart",
        "img": "https://img.shields.io/badge/deps%20in%202D-see_here-08f0fd.svg?style=flat-square",
        "url": "http://npm.anvaka.com/#/view/2d/%REPONAME%"
      }
    },
    {
      "downloads": {
        "alt": "Downloads/Month",
        "img": "https://img.shields.io/npm/dm/%REPONAME%.svg?style=flat-square",
        "url": "https://npmcharts.com/compare/%REPONAME%"
      }
    },
    {
      "runkit": {
        "alt": "Test in browser",
        "img": "https://img.shields.io/badge/runkit-test_in_browser-a853ff.svg?style=flat-square",
        "url": "https://npm.runkit.com/%REPONAME%"
      }
    },
    {
      "prettier": {
        "alt": "Code style: prettier",
        "img": "https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square",
        "url": "https://prettier.io"
      }
    },
    {
      "contributors": {
        "alt": "All Contributors",
        "img": "https://img.shields.io/badge/all_contributors-%CONTRIBUTORCOUNT%-orange.svg?style=flat-square",
        "url": "#contributors"
      }
    },
    {
      "license": {
        "alt": "MIT License",
        "img": "https://img.shields.io/badge/licence-MIT-51c838.svg?style=flat-square",
        "url": "https://bitbucket.org/codsen/codsen/src/master/LICENSE"
      }
    }
  ],
  "contributing": {
    "header": "## Contributing",
    "restofit": "* If you see an error, [raise an issue](%ISSUELINK%).\n* If you want a new feature but can't code it up yourself, also [raise an issue](%ISSUELINK%). Let's discuss it.\n* If you tried to use this package, but something didn't work out, also [raise an issue](%ISSUELINK%). We'll try to help.\n* If you want to contribute some code, fork the [monorepo](https://bitbucket.org/codsen/codsen/src/) via BitBucket, then write code, then file a pull request via BitBucket. We'll merge it in and release.\n\nIn monorepo, npm libraries are located in `packages/` folder. Inside, the source code is located either in `src/` folder (normal npm library) or in the root, `cli.js` (if it's a command line application).\n\nThe npm script \"`dev`\", the `\"dev\": \"rollup -c --dev --silent\"` builds the development version retaining all `console.log`s with row numbers. It's handy to have [js-row-num-cli](https://www.npmjs.com/package/js-row-num-cli) installed globally so you can automatically update the row numbers on all `console.log`s."
  },
  "contributors": {
    "content_above_table": "Thanks goes to these wonderful people (hover the cursor over contribution icons for a tooltip to appear):",
    "content_below_table": "This project follows the [all contributors][all-contributors-url] specification.\nContributions of any kind are welcome!",
    "links": {
      "all-contributors-url": "https://github.com/kentcdodds/all-contributors"
    }
  },
  "eslintrc": {
    "env": {
      "es6": true,
      "node": true
    },
    "extends": [
      "eslint:recommended",
      "plugin:prettier/recommended"
    ],
    "parserOptions": {
      "ecmaVersion": 2018,
      "sourceType": "module"
    },
    "plugins": [
      "ava",
      "no-unsanitized",
      "import"
    ],
    "root": true,
    "rules": {
      "ava/assertion-arguments": "error",
      "ava/max-asserts": [
        "off",
        5
      ],
      "ava/no-async-fn-without-await": "error",
      "ava/no-cb-test": "off",
      "ava/no-duplicate-modifiers": "error",
      "ava/no-identical-title": "error",
      "ava/no-invalid-end": "error",
      "ava/no-nested-tests": "error",
      "ava/no-only-test": "error",
      "ava/no-skip-assert": "error",
      "ava/no-skip-test": "error",
      "ava/no-statement-after-end": "error",
      "ava/no-todo-implementation": "error",
      "ava/no-todo-test": "warn",
      "ava/no-unknown-modifiers": "error",
      "ava/prefer-async-await": "error",
      "ava/prefer-power-assert": "off",
      "ava/test-ended": "error",
      "ava/test-title": [
        "error",
        "if-multiple"
      ],
      "ava/use-t": "error",
      "ava/use-t-well": "error",
      "ava/use-test": "error",
      "ava/use-true-false": "error",
      "curly": "error",
      "import/no-extraneous-dependencies": [
        "error",
        {
          "devDependencies": [
            "**/*test.js",
            "test/**/*.*",
            "rollup.config.js"
          ]
        }
      ],
      "no-console": "off",
      "no-constant-condition": [
        "error",
        {
          "checkLoops": false
        }
      ],
      "no-else-return": "error",
      "no-inner-declarations": "error",
      "no-unneeded-ternary": "error",
      "no-useless-return": "error",
      "no-var": "error",
      "one-var": [
        "error",
        "never"
      ],
      "prefer-arrow-callback": "error",
      "prefer-const": "error",
      "prefer-template": "error",
      "strict": "error",
      "symbol-description": "error",
      "yoda": [
        "error",
        "never",
        {
          "exceptRange": true
        }
      ]
    }
  },
  "files": {
    "delete": [
      ".gitignore",
      ".editorconfig",
      ".gitattributes",
      ".bithoundrc",
      ".travis.yml",
      "bitbucket-pipelines.yml"
    ],
    "write_hard": [
      {
        "contents": "",
        "name": ""
      },
      {
        "contents": "dist/\ntest/fixtures\nfixtures/\n*.json\n",
        "name": ".prettierignore"
      },
      {
        "contents": "package-lock=false\n",
        "name": ".npmrc"
      },
      {
        "contents": "MIT License\n\nCopyright (c) 2015-%YEAR% Roy Revelt and other contributors\n\nPermission is hereby granted, free of charge, to any person obtaining\na copy of this software and associated documentation files (the\n\"Software\"), to deal in the Software without restriction, including\nwithout limitation the rights to use, copy, modify, merge, publish,\ndistribute, sublicense, and/or sell copies of the Software, and to\npermit persons to whom the Software is furnished to do so, subject to\nthe following conditions:\n\nThe above copyright notice and this permission notice shall be\nincluded in all copies or substantial portions of the Software.\n\nTHE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,\nEXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF\nMERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND\nNONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE\nLIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION\nOF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION\nWITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.",
        "name": "LICENSE"
      }
    ],
    "write_soft": [
      {
        "contents": "",
        "name": ""
      }
    ]
  },
  "footer": {
    "alwaysAddTheseLinks": {
      "all-contributors": "https://github.com/kentcdodds/all-contributors",
      "emojis": "https://github.com/kentcdodds/all-contributors#emoji-key"
    }
  },
  "header": {
    "dontQuoteDescription": true,
    "rightFloatedBadge": []
  },
  "licence": {
    "extras": [],
    "header": "## Licence",
    "restofit": "MIT License\n\nCopyright (c) 2015-%YEAR% Roy Revelt and other contributors"
  },
  "npmignore": {
    "badFiles": [
      ".babelrc",
      ".editorconfig",
      ".eslintrc",
      ".eslintrc.json",
      ".gitattributes",
      ".gitignore",
      ".npmignore",
      ".prettierignore",
      ".travis.yml",
      "bitbucket-pipelines.yml",
      "chlu_adds_missing_diff_links.gif",
      "cli_bak.js",
      "contributing.md",
      "feature1.gif",
      "ideas.md",
      "rollup.config.js",
      "test.js",
      "test.zip",
      "test_bak.js",
      "travis.js"
    ],
    "badFolders": [
      ".idea",
      ".nyc_output",
      "coverage",
      "fixtures",
      "media",
      "src",
      "temp",
      "test",
      "test-folder"
    ],
    "goodFiles": [
      "cli-es5.js",
      "cli.js",
      "index-es5.js",
      "index.js",
      "init-npmignore.js",
      "runkit_example.js",
      "util.js"
    ],
    "goodFolders": [
      "bin",
      "dist",
      "lib",
      "test_alt"
    ]
  },
  "package": {
    "ava": {
      "compileEnhancements": false,
      "require": [
        "esm"
      ],
      "verbose": true
    },
    "devDependencies": {
      "@babel/core": "^7.2.2",
      "@babel/preset-env": "^7.2.3",
      "@babel/register": "^7.0.0",
      "ava": "^1.0.1",
      "babel-plugin-istanbul": "^5.1.0",
      "eslint": "^5.11.1",
      "eslint-config-prettier": "^3.3.0",
      "eslint-plugin-ava": "^5.1.1",
      "eslint-plugin-import": "^2.14.0",
      "eslint-plugin-no-unsanitized": "^3.0.2",
      "eslint-plugin-prettier": "^3.0.1",
      "nyc": "^13.1.0",
      "prettier": "^1.15.3",
      "rollup": "^1.0.0",
      "rollup-plugin-babel": "^4.2.0",
      "rollup-plugin-cleanup": "^3.1.0",
      "rollup-plugin-commonjs": "^9.2.0",
      "rollup-plugin-json": "^3.1.0",
      "rollup-plugin-license": "^0.7.0",
      "rollup-plugin-node-resolve": "^4.0.0",
      "rollup-plugin-strip": "^1.2.1",
      "rollup-plugin-terser": "^3.0.0"
    },
    "engines": {
      "node": ">=8.9"
    },
    "esm": {
      "await": true,
      "cjs": true
    },
    "husky": {
      "hooks": {
        "pre-commit": "npm run format && npm test"
      }
    },
    "license": "MIT",
    "nyc": {
      "instrument": false,
      "require": [
        "@babel/register"
      ],
      "sourceMap": false
    }
  },
  "rollup": {
    "infoTable": {
      "cjsTitle": "Main export - **CommonJS version**, transpiled to ES5, contains `require` and `module.exports`",
      "esmTitle": "**ES module** build that Webpack/Rollup understands. Untranspiled ES6 code with `import`/`export`.",
      "umdTitle": "**UMD build** for browsers, transpiled, minified, containing `iife`'s and has all dependencies baked-in"
    },
    "infoTitle": "Here's what you'll get:"
  },
  "scripts": {
    "cli": {
      "coverage": "nyc report --reporter=json-summary",
      "format": "prettier '*.{js,ts,css,less,scss,vue,gql,md}' --write && npm run lint",
      "lect": "lect",
      "lint": "./node_modules/.bin/eslint **/*.js --fix",
      "test": "npm run lint && npm run unittest",
      "unittest": "./node_modules/.bin/nyc ava && npm run coverage"
    },
    "rollup": {
      "build": "rm -rf dist && rollup -c",
      "coverage": "nyc report --reporter=json-summary",
      "dev": "rm -rf dist && rollup -c --dev --silent",
      "format": "prettier '*.{js,ts,css,less,scss,vue,gql,md}' --write && npm run lint",
      "lect": "lect",
      "lint": "./node_modules/.bin/eslint \"**/*.js\" --fix --ignore-pattern \"dist/*\" --ignore-pattern \"rollup.config.js\"",
      "prepare": "npm run build",
      "pretest": "npm run build",
      "test": "npm run lint && npm run unittest",
      "unittest": "./node_modules/.bin/nyc ava && npm run coverage",
      "version": "npm run build && git add ."
    }
  },
  "travis": false,
  "various": {
    "addBlanks": true,
    "back_to_top": {
      "enabled": true,
      "label": "⬆  back to top",
      "url": "#markdown-header-%REPONAME%"
    }
  }
}
