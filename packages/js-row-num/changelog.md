# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 1.3.0 (2019-01-20)

- ✨ Various documentation and setup tweaks after we migrated to monorepo
- ✨ Setup refresh: updated dependencies and all config files using automated tools

## 1.1.0 (2018-12-26)

- ✨ `String.padStart` is Ecmascript 2017 which limits the Node versions we can support, so we replaced it ([5a49a2e](https://bitbucket.org/codsen/codsen/src/master/packages/js-row-num/commits/5a49a2e))

## 1.0.0 (2017-07-11)

- ✨ First public release
